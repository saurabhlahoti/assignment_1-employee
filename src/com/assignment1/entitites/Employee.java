package com.assignment1.entitites;

public class Employee {
	public int id;
	public String name;
	public float salary;

	public String toString() {
		return "ID " + id + " \nName : " + name + " \nSalary : " + salary;
	}
}
