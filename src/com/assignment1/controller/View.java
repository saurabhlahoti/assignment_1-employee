package com.assignment1.controller;
import java.util.*;
import java.io.*;
import com.assignment1.entitites.*;
import com.assignment1.util.*;
public class View {

	public static void main(String[] args) throws java.lang.Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));

		int count = 1, choiceInt = 0, response = 0;
		//Employee emp = new Employee();
		ArrayList<Employee> empList = new ArrayList<Employee>();
		do {
			System.out
					.println("1. Enter data \n 2. Get By ID \n 3. Get by Name\n 4. Sort by Name \n 5. Exit");
			choiceInt = Integer.parseInt(reader.readLine());
			if (choiceInt < 1 || choiceInt > 5) {
				System.out.print("System Terminating ...");
				System.exit(0);
			}

			choiceInt--;
			Choice ch = Choice.values()[choiceInt];
			switch (ch) {

			case Add:

				while (true) {
					Employee emp_now = new Employee();
					System.out.println("Enter name :");
					emp_now.id = count;
					emp_now.name = reader.readLine();
					System.out.println("Enter salary :");
					emp_now.salary = Float.parseFloat(reader.readLine());
					count++;
					empList.add(emp_now);
					System.out
							.println("Do you want to enter more data if yes Enter 1 : ");
					response = Integer.parseInt(reader.readLine());
					if (response != 1)
						break;
				}
				break;

			case Get_By_ID:
				int searchId;
				System.out.println("Enter ID to be searched :");
				searchId = Integer.parseInt(reader.readLine());
				getById(empList, searchId);
				break;

			case Get_By_Name:
				String searchName;
				System.out.println("Enter name to be searched :");
				searchName = reader.readLine();
				getByName(empList, searchName);
				break;

			case Sort_By_Name:
				sortByName(empList);
				break;

			case Exit:
				System.exit(0);
				break;

			default:
				break;
			}

		} while (choiceInt != 5);

	}
	static void getById(ArrayList<Employee> empList, int searchId) {

		for (int i = 0; i < empList.size(); i++) {
			Employee emp = (Employee) empList.get(i);

			if (searchId == emp.id) {
				display(emp);
				break;
			}
		}
	}

	static void getByName(ArrayList<Employee> empList, String search_name) {
		for (int i = 0; i < empList.size(); i++) {
			Employee emp = (Employee) empList.get(i);
			if (search_name.equals(emp.name)) {
				display(emp);
				break;
			}
		}
	}

	private static Comparator<Employee> byName() {
		return new Comparator<Employee>() {
			// @Override
			public int compare(Employee emp1, Employee emp2) {
				return emp1.name.compareTo(emp2.name);
			}
		};
	}

	static void sortByName(ArrayList<Employee> empList) {

		Collections.sort(empList, byName());
		for (int i = 0; i < empList.size(); i++) {
			Employee emp = (Employee) empList.get(i);
			display(emp);
		}
	}

	static void display(Employee emp){
		System.out.println(emp);
		
	}
	
	

}
